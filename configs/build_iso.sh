#!/usr/bin/env bash

set -ex

BASEDIR=$(readlink -f "$(dirname "$0")/..")
HOSTNAME=$1
# Allow to define the intermediary IPXE_TARGET folder. This can pose problem
# on old hardware (e.g. beaubourg generated iso would not boot on the default
# path, we had to rename it to bin/ipxe.iso )
IPXE_TARGET=${2-bin-x86_64-efi/ipxe.iso}

CONFIG="${BASEDIR}/configs/${HOSTNAME}.yaml"
IPXE_SCRIPT="${BASEDIR}/configs/${HOSTNAME}.ipxe"
PASSWORDS_FILE="${BASEDIR}/configs/passwords"

if ! [ -f "${CONFIG}" ]; then
    echo "Missing config for host ${HOSTNAME}: ${CONFIG}" 1>&2
    exit 2
fi

TMPDIR="$(mktemp -d)"
trap 'rm -rf -- "${TMPDIR}"' EXIT

j2 -f yaml ${BASEDIR}/configs/template.ipxe.j2 "${CONFIG}" >"${IPXE_SCRIPT}"

make -C "${BASEDIR}/src" -j8 "${IPXE_TARGET}" EMBED="${IPXE_SCRIPT}" DEBUG=eth_slow NO_WERROR=1
mv "${BASEDIR}/src/${IPXE_TARGET}" "${BASEDIR}/configs/${HOSTNAME}.iso"

echo "built iso image ${BASEDIR}/configs/${HOSTNAME}.iso" 1>&2

[[ -e "${PASSWORDS_FILE}" ]] &&
    PASSWORD=$(awk -F: "\$1 == \"${HOSTNAME}\" { print \$2 }" ${PASSWORDS_FILE})

if [ -z "$PASSWORD" ]; then
    PASSWORD="$(xkcdpass -d- -a "${HOSTNAME:0:4}")"

    echo "${HOSTNAME}:${PASSWORD}" >>${PASSWORDS_FILE}
fi

CRYPTED_PASSWORD="$(echo "${PASSWORD}" | /usr/bin/mkpasswd -s)"

PRESEED_DIR="${BASEDIR}/configs/preseeding"
PRESEED_FILE="${PRESEED_DIR}/${HOSTNAME}.txt"

env CRYPTED_PASSWORD="$CRYPTED_PASSWORD" \
    j2 -f yaml -e env "${PRESEED_DIR}/preseed.txt.j2" "${CONFIG}" >"${PRESEED_FILE}"

FINISH_INSTALL_DIR="${PRESEED_DIR}/finish_install"
FINISH_INSTALL="${FINISH_INSTALL_DIR}/${HOSTNAME}.sh"

j2 -f yaml -e env "${FINISH_INSTALL_DIR}/finish_install.sh.j2" "${CONFIG}" >"${FINISH_INSTALL}"

echo "Generated preseeding config in ${PRESEED_FILE} and ${FINISH_INSTALL}." 1>&2
