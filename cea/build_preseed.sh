#!/usr/bin/env bash

set -e

BASEDIR=$(readlink -f "$(dirname "$0")")
HOSTNAME=$1

CONFIG="${BASEDIR}/configs/${HOSTNAME}.yaml"
ROOT_PASSWORDS_FILE="${BASEDIR}/root_passwords"
SWH_PASSWORDS_FILE="${BASEDIR}/swh_passwords"

mkdir -p ${BASEDIR}/configs/preseeding/finish_install

generate_and_save_password() {
    local PASSWORDS_FILE=$1
    [[ -e "${PASSWORDS_FILE}" ]] && PASSWORD=$(awk -F: "\$1 == \"${HOSTNAME}\" { print \$2 }" "${PASSWORDS_FILE}")
    if [[ -z "${PASSWORD}" ]]; then
        PASSWORD="$(xkcdpass -d- -a "${HOSTNAME:0:4}")"

        echo "${HOSTNAME}:${PASSWORD}" >>${PASSWORDS_FILE}
    fi
    echo "${PASSWORD}"
}

if ! [ -f "${CONFIG}" ]; then
    echo "Missing config for host ${HOSTNAME}: ${CONFIG}" 1>&2
    exit 2
fi

ROOT_PASSWORD="$(generate_and_save_password ${ROOT_PASSWORDS_FILE})"
SWH_PASSWORD="$(generate_and_save_password ${SWH_PASSWORDS_FILE})"

CRYPTED_ROOT_PASSWORD="$(echo "${ROOT_PASSWORD}" | mkpasswd -s)"
CRYPTED_SWH_PASSWORD="$(echo "${SWH_PASSWORD}" | mkpasswd -s)"

PRESEED_FILE="${BASEDIR}/configs/preseeding/${HOSTNAME}.txt"
env CRYPTED_ROOT_PASSWORD="${CRYPTED_ROOT_PASSWORD}" \
    CRYPTED_SWH_PASSWORD="${CRYPTED_SWH_PASSWORD}" \
    j2 -f yaml -e env "${BASEDIR}/preseed.txt.j2" \
       "${CONFIG}" >"${PRESEED_FILE}"

FINISH_INSTALL="${BASEDIR}/configs/preseeding/finish_install/${HOSTNAME}.sh"
j2 -f yaml -e env "${BASEDIR}/finish_install.sh.j2" "${CONFIG}" >"${FINISH_INSTALL}"

echo "Generated preseeding config in ${PRESEED_FILE} and ${FINISH_INSTALL}." 1>&2
