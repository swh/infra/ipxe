# Preseed configuration for CEA servers

## Interesting links
https://preseed.debian.net/debian-preseed/bookworm/amd64-main-full.txt

## Prepare the configuration

### IPXE preparation

On the CEA ipxe servers, the ipxe boot is managed by the debian
`debian-installer-12-netboot-<arch>` package.  Before starting an
installation, ensure the debian package is up-to-date:

```
apt update
apt install debian-installer-12-netboot-amd64
```

### Hardware preparation

#### HP Servers

In the raid card management tool, create the logical volume for boot in RAID1.
Retrieve the disk UUID in the raid card tool.

For HP servers, it can only be done in the bios management, not directly in
the iLo interface.

Go in the bios (`F9` during boot screen), select `System Configuration`, first
raid card `MR216i` or equivalent, in `Actions` section, select `Configure`,
`Create logical drive`.

Raid configuration:
- RAID1
- 2 disks, usually 480GB
- Logical Name: `SYSTEM`
- everything else by default

Then confirm and validate

**Before rebooting**, in the iLo web interface, get the `Identifier.Durable`
name: ![durable name](docs/iLo-raid-durable_name.png)

Breadcrumbs:
> "System Information"
> Tab "Storage"
> Newly storage configured (e.g. "HPE MR216i-p Gen10+")
> Click on "Volume xxx"
> panel "Volume detail" appears
> toggle "details"
> Copy value of "Identifier.Durable" in the clipboard

This value must be prefixed with `wwn-0x` to specify the
`BOOT_DISK_ID_PATTERN` value in the config file.

For example: `BOOT_DISK_ID_PATTERN: wwn-0x600063B40DD553802E296F4A1DB5BC1F`

### Preseed generation

For the following part, we'll be working on a node to setup called $NODE.
For example:

```
$ NODE=balin024
```

1. Create a yaml file with the server properties in the ``configs`` directory

    For example:

```
$ head -3 cea/configs/$NODE.yaml
---
HOSTNAME: balin024
```

2. Set the passwords in `root_passwords` and `swh_passwords` files with the
   respective values in the credential store

3. Launch the `build_preseed.sh` script. It will generate the preseed file and
   the post install script in the `preseeding` and `preseeding/finish_install`
   directories

   For example:

```
./cea/build_preseed.sh $NODE
```

4. Transfer the `preseeding` directory on the bastion in the directory
   `/var/www/html/preseeding`

   For example:

```
$ BASTION=swh@angrenost.internal.cea.swh.network
$ rsync -avP ./cea/configs/preseeding $BASTION:/var/www/html/
```

5. Reboot the server with a <network card> IPV4 PXE boot device (**not IPV4
   http IPXE**)

6. When the grub menu is displayed, select `Advanced Options / Automated
   install`

7. Select any interface when asked

8. The next step is to provide the preseed file path.
   With balin022 example, enter: `http://10.25.6.254/preseeding/balin022.txt`

9. After the reboot, launch ansible (follow the ansible readme for more
   details on the prerequisites for this part)
