#ifndef GENERAL_H
#define GENERAL_H

#undef NET_PROTO_IPV6

#undef HTTP_AUTH_BASIC
#undef HTTP_AUTH_DIGEST

#undef CRYPTO_80211_WEP
#undef CRYPTO_80211_WPA
#undef CRYPTO_80211_WPA2

#define NSLOOKUP_CMD
#define VLAN_CMD
#define REBOOT_CMD
#define PARAM_CMD
#define PING_CMD
#define IPSTAT_CMD

#endif
